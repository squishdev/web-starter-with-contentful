# README #

### What is this repository for? ###

* Quick summary
This is a repo to get a react/typescript project up very quickly. The project could be used for code testing or building any basic react web project.
* Version
1.0.0


### How do I get set up? ###
* Clone Web Starter
* npm install
* can see code in browser with localhost

### Who do I talk to? ###
* Repo owner or admin
George Roots
squish.roots@gmail.com

### Other community or team contact
* Steve Hansell
* Mohammed Siddeeq
* Tyler Weiss
* Marcus Warnsley