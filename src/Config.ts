/*
  Helper to get environment configs. See the DefinePlugin in webpack.config files
*/
/* tslint:disable */
export abstract class Options {
  abstract assetsUrl(): string;
  abstract baseApiUrl(): string;
  abstract environment(): string;
}

const cdnUrl = "http://resources.dotfetch.io";

class DevOptions extends Options {
  assetsUrl() { return cdnUrl; }
  baseApiUrl() { return "http://localhost:8081"; }
  environment() { return "dev"; }
}

class IntOptions extends Options {
  assetsUrl() { return cdnUrl; }
  baseApiUrl() { return ""; }
  environment() { return "int"; }
}

class UatOptions extends Options {
  assetsUrl() { return cdnUrl; }
  baseApiUrl() { return ""; }
  environment() { return "uat"; }
}

class PrdOptions extends Options {
  assetsUrl() { return cdnUrl; }
  baseApiUrl() { return ""; }
  environment() { return "prd"; }
}
/* tslint:enable */

let opts: Options;

switch (ENV) {
  case "prd":
    opts = new PrdOptions();
    break;
  case "uat":
    opts = new UatOptions();
    break;
  case "int":
    opts = new IntOptions();
    break;
  case "dev":
    opts = new DevOptions();
    break;
  default:
    console.warn("Setting to default DEV options - NOT FOR PRODUCTION");
    opts = new DevOptions();
}

export class Config {
  static get ASSETS_URL(): string {
    return opts.assetsUrl();
  }

  static get BASE_API_URL(): string {
    return opts.baseApiUrl();
  };

  static get ENVIRONMENT(): string {
    return opts.environment();
  }

  static get ALL_VALUES(): Object {
    return {
      assetsUrl: opts.assetsUrl(),
      baseApiUrl: opts.baseApiUrl(),
    };
  }
}
