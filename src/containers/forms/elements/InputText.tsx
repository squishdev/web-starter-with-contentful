import * as React from "react";
import { TextBox, TextBoxProps } from "../../../components/forms/elements/TextBox";


interface TextBoxState {
    goodValidation: string;
}

export class InputText extends React.Component<TextBoxProps, TextBoxState> {
    constructor(props: any) {
        super(props);
        this.state = {
            goodValidation: ""
        }
    }

    public render() {
        return (
            <TextBox onBlur={(e: any) => {this.handleBlur(e)}} name={this.props.name}
                     type={this.props.type} altValue={this.props.altValue} passVal={this.state.goodValidation } />
        );
    }

    private handleBlur (e: any) {
        let judgeVal = "";
        if (this.props.validate && e.target.value === "") {
            judgeVal = "fail";
        } else {
            judgeVal= " pass";
        }
        this.setState(() => {
            return {
                goodValidation: judgeVal
            }
        })
    }
}